/*
 * This source code is Copyright 2015 by Tuangrat Mungmeerattanaworachot.
 */

/**
 * Counting the vowel syllables of the word.
 * @author Tuangrat Mungmeerattanaworachot 5710546241
 * @version 2015.03.24
 */
public class WordCounter {

	/** Condition for checking what kind of char it is. */
	private State state;
	
	/** Amount of syllables. */
	private int syllables;

	/**
	 * Checking a letter.
	 * @param c is the character of the word
	 * @return true if the character is a letter, 
	 * 		   otherwise return false
	 */
	private boolean isLetter(char c) {
		return Character.isLetter(c);
	} 

	/**
	 * Checking a vowel.
	 * @param c is the character of the word
	 * @return true if the index of the first occurrence 
	 * 		   of the character in the character sequence 
	 * 		   is greater or equals 0, otherwise return -1
	 */
	private boolean isVowel(char c) {
		return ("AEIOUYaeiouy".indexOf(c) >= 0);
	}
	
	/**
	 * Checking a character "E or e".
	 * @param c is the character of the word
	 * @return true if the index of the first occurrence 
	 * 		   of the character in the character sequence 
	 * 		   is greater or equals 0, otherwise return -1
	 */
	private boolean isE(char c) {
		return ("Ee".indexOf(c) >= 0);
	}

	/**
	 * Checking dash "-".
	 * @param c
	 * @return true if the index of the first occurrence 
	 * 		   of the character in the character sequence 
	 * 		   is greater or equals 0, otherwise return -1
	 */
	private boolean isDash(char c) {
		return ("-".indexOf(c) >= 0);
	}
	
	/**
	 * Setting a state of the character.
	 * @param newState is the state of the character
	 */
	public void setState(State newState) {
		if(newState != state) newState.enterState();
		this.state = newState;
	}
	
	/** Starting state. */
	State STARTSTATE = new State() {

		@Override
		public void handleChar(char c) {
			if(isE(c)) setState(ESTATE);
			else if(isVowel(c)) setState(VOWELSTATE);
			else if(isLetter(c)) setState(CONSONANTSTATE);
			else if(isDash(c)) setState(NONWORDSTATE);
			else setState(NONWORDSTATE);
		}

		@Override
		public void enterState() {
			
		}

	};

	/** Consonant state. */
	State CONSONANTSTATE = new State() {

		@Override
		public void handleChar(char c) {
			if(isE(c)) setState(ESTATE);
			else if(isVowel(c)) setState(VOWELSTATE);
			else if(isLetter(c)) setState(CONSONANTSTATE);
			else if(isDash(c)) setState(DASHSTATE);
			else setState(NONWORDSTATE);
		}

		@Override
		public void enterState() {

		}

	};

	/** Vowel state. */
	State VOWELSTATE = new State() {

		@Override
		public void handleChar(char c) {
			if(isVowel(c) && !("Yy".indexOf(c) >= 0));
			else if(isLetter(c)) setState(CONSONANTSTATE);
			else if(isDash(c)) setState(DASHSTATE);
			else setState(NONWORDSTATE);
		}

		@Override
		public void enterState() {
			syllables++;
		}

	};

	/** E state. */
	State ESTATE = new State() {
		
		@Override
		public void handleChar(char c) {
			if(isVowel(c) && !("Yy".indexOf(c) >= 0)) setState(VOWELSTATE);
			else if(isE(c));
			else if(isLetter(c)) {
				syllables++;
				setState(CONSONANTSTATE);
			}
			else if(isDash(c)) setState(DASHSTATE);
			else setState(NONWORDSTATE);
		}

		@Override
		public void enterState() {
			
		}
		
	};

	/** Dash state. */
	State DASHSTATE = new State() {

		@Override
		public void handleChar(char c) {
			if(isVowel(c)) setState(VOWELSTATE);
			else if(isE(c)) setState(ESTATE);
			else if(isLetter(c)) setState(CONSONANTSTATE);
			else if(isDash(c)) setState(NONWORDSTATE);
			else setState(NONWORDSTATE);
		}

		@Override
		public void enterState() {
			
		}

	};

	/** Non-word state. */
	State NONWORDSTATE = new State() {

		@Override
		public void handleChar(char c) {

		}

		@Override
		public void enterState() {
			
		}
	};
	
	/**
	 * Counting the syllables of the word. 
	 * @param word is a full word
	 * @return the amount of syllables
	 */
	public int countSyllable(String word) {
		state = STARTSTATE;
		syllables = 0;
		for(int k=0; k<word.length(); k++) {
			char c = word.charAt(k);
			state.handleChar(c);
			if(k==0 && state==DASHSTATE) break;
		}
		
		if (state==DASHSTATE) return 0;
		else if(state==ESTATE && syllables == 0) {
			syllables++;
		}
		
		return syllables;
	}

}
