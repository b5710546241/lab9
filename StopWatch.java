/*
 * This source code is Copyright 2015 by Tuangrat Mungmeerattanaworachot.
 */

/**
 * A stopwatch that measure the time between 
 * starting time and ending time.
 * @author Tuangrat Mungmeerattanaworachot 5710546241
 * @version 2015.02.02
 */
public class StopWatch {

	/** Starting time. */
	private long start;
	
	/** Ending time. */
	private long stop;
	
	/** Status of stopwatch. */
	private boolean timerun = false;

	/**
	 * Getting the elapsed time.
	 * @return elapsed time
	 */
	public double getElapsed() {
		if(isRunning())
			return (System.nanoTime()-start)*Math.pow(10, -9);
		else
			return (stop-start)*Math.pow(10, -9);
		
	}
	
	/**
	 * Check status of the stopwatch.
	 * @return true if the stopwatch is running
	 */
	public boolean isRunning() {
		return timerun;
	}

	/**
	 * Start timing.
	 */
	public void start() {
		if(!isRunning()) {
			start = System.nanoTime();
			timerun = true;
		}
	}

	/**
	 * Stop timing.
	 */
	public void stop() {
		if(isRunning()) {
			stop = System.nanoTime();
			timerun = false;
		}
	}
}
