/*
 * This source code is Copyright 2015 by Tuangrat Mungmeerattanaworachot.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Reads all the words from a URL or File.
 * @author Tuangrat Mungmeerattanaworachot 5710546241
 * @version 2015.03.24
 */
public class Main {
	
	/**
	 * Reading the words from a URL or File.
	 * Compute the elapsed time (use your StopWatch), 
	 * and print the results on the console.
	 * @param args is not used
	 */
	public static void main(String[] args) {
		StopWatch stopwatch = new StopWatch();
		stopwatch.start();
		int syllables = 0;
		WordCounter wordcounter = new WordCounter();
		final String DICT_URL = "http://se.cpe.ku.ac.th/dictionary.txt";
		URL url = null;
		int wordcount = 0;
		
		try {
			url = new URL( DICT_URL );
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		}
		
		try {
			InputStream in = url.openStream( );
			BufferedReader breader = new BufferedReader(new InputStreamReader(in));
			while(true) {
				String line = breader.readLine();
				if(line == null) break;
				line = line.trim();
				syllables += wordcounter.countSyllable(line);
				wordcount++;
			}
		} catch (IOException e2) {
			e2.printStackTrace();
		}
		stopwatch.stop();
		System.out.printf("Reading words from %s\n", DICT_URL);
		System.out.printf("Counted %,d syllables in %,d words\n", syllables, wordcount);
		System.out.printf("Elapsed time: %.3f sec", stopwatch.getElapsed());
		
	}
}
