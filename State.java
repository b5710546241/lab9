/*
 * This source code is Copyright 2015 by Tuangrat Mungmeerattanaworachot.
 */

/**
 * Interface that used for checking a kind of a character.
 * @author Tuangrat Mungmeerattanaworachot 5710546241
 * @version 2015.03.24
 */
public interface State {

	/** Checking each states of a character. 
	 *	@param c is the character of the word
	 */
	public void handleChar(char c);
	
	/** Counting an amount of vowel syllable in each words. */
	public void enterState();
	
}
